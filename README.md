# WARSHIPS

A school project game written in C.

It is based on ncurses library and features advanced console display features like:
* `mvprintw`
* `attron(A_REVERSE)`;

Bugs included free of cost.


## Build instructions

Use a gcc compiler. Link `ncurses`.

```bash
gcc -o warships warships.c -lncurses
./warships
```

A nice touch is to have ncurses up to date. On macOS use [Homebrew](https://brew.sh):

```bash
brew install ncurses
export LDFLAGS="-L/usr/local/opt/ncurses/lib"
export CPPFLAGS="-I/usr/local/opt/ncurses/include"
```

Do this before compiling Warships.


## Changelog

2019-04-09
* Initial commit
* Added README.md
* Added colour functionality

2019-04-10
* Added LICENSE

## License

MIT License provided in `LICENSE` file.
