#include <ncurses.h>
#include <string.h>

typedef struct Coord
{
	int x;
	int y;
}
Coord;

int windowRows, windowCols;

void mapClear(char map[10][10])	/* Fill entire map with "." */
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			map[i][j] = '.';
		}
	}
}

void mapDraw(char map[10][10], char map2[10][10], int mode)
{
	clear();
	switch (mode) {
		case 1:
		{
			mvprintw(windowRows/2-10,(windowCols-21)/2, "x 0 1 2 3 4 5 6 7 8 9");
			for (int i = 0; i < 10; i++)
			{
				mvprintw(windowRows/2-9+i,(windowCols-21)/2, "%c ", i+65);
				for (int j = 0; j < 10; j++)
				{
					if (map[i][j] == '#')
						attron(A_REVERSE  | COLOR_PAIR(2));
					if (map[i][j] == 'X')
						attron(A_REVERSE  | COLOR_PAIR(1));
					if (map[i][j] == 'O')
						attron(A_BOLD);
					printw("%c ", map[i][j]);
					attroff(A_BOLD);
					attroff(A_REVERSE);
					attroff(COLOR_PAIR(1));
					attroff(COLOR_PAIR(2));
				}
			}
			break;
		}
		case 2:
		{
			mvprintw(windowRows/2-10,(windowCols-21)/4, "x 0 1 2 3 4 5 6 7 8 9");
			for (int i = 0; i < 10; i++)
			{
				mvprintw(windowRows/2-9+i,(windowCols-21)/4, "%c ", i+65);
				for (int j = 0; j < 10; j++)
				{
					if (map[i][j] == '#')
						attron(A_REVERSE  | COLOR_PAIR(2));
					if (map[i][j] == 'X')
						attron(A_REVERSE  | COLOR_PAIR(1));
					if (map[i][j] == 'O')
						attron(A_BOLD);
					printw("%c ", map[i][j]);
					attroff(A_BOLD);
					attroff(A_REVERSE);
					attroff(COLOR_PAIR(1));
					attroff(COLOR_PAIR(2));
				}
			}
			mvprintw(windowRows/2-10,(windowCols-21)*3/4, "x 0 1 2 3 4 5 6 7 8 9");
			for (int i = 0; i < 10; i++)
			{
				mvprintw(windowRows/2-9+i,(windowCols-21)*3/4, "%c ", i+65);
				for (int j = 0; j < 10; j++)
				{
					if (map2[i][j] != '#')
					{
						if (map2[i][j] == 'X')
							attron(A_REVERSE  | COLOR_PAIR(1));
						if (map2[i][j] == 'O')
							attron(A_BOLD);
						printw("%c ", map2[i][j]);
						attroff(A_BOLD);
						attroff(A_REVERSE);
						attroff(COLOR_PAIR(1));
						attroff(COLOR_PAIR(2));
					}
					else
					{
						printw(". ");
					}
				}
			}
		}
	}
	refresh();
}

void mapPut(char map[10][10], Coord coord)
{
	map[coord.x][coord.y] = '#';
}

void mapShoot(char map[10][10], Coord coord)
{
	if (map[coord.x][coord.y] == '#' )
		map[coord.x][coord.y] = 'X';
	else if (map[coord.x][coord.y] == '.' )
		map[coord.x][coord.y] = 'O';
}

Coord getCoords() {
	Coord coord;
	char letter[3];
	mvprintw(windowRows-4,(windowCols-21)/2, "Input coordinates: __");
	refresh();
	curs_set(1);
	move(windowRows-4,((windowCols-21)/2)+19);
	scanw(" %c%c", &letter[0], &letter[1]);
	curs_set(0);
	// if (!
	// {
	// 	printw("No coordinates passed\n");
	// 	return getCoords();
	// }
	coord.y = letter[1] - 48;

	if (coord.y < 0 || coord.y > 9)
	{
		mvprintw(windowRows-3,(windowCols-17)/2, "Wrong coordinates");
		refresh();
		return getCoords();
	}

	if (letter[0] >= 65 && letter[0] <= 74)
	{
		coord.x = letter[0] - 65;
	}
	else if (letter[0] >= 97 && letter[0] <= 106)
	{
		coord.x = letter[0] - 97;
	}
	else
	{
		mvprintw(windowRows-3,(windowCols-17)/2, "Wrong coordinates");
		refresh();
		return getCoords();
	}
	return coord;
}

int gameCheck(char map[10][10])	/* Iterate through the map looking for any ships left */
{				/* Returns 1 on "found ship" and 0 on "no ships" */
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			if (map[i][j] == '#')
				return 1;
		}
	}
	return 0;
}

void intro()
{
	curs_set(0);	/* disable cursor */
	attron(A_REVERSE);	/* enable reverse terminal attribute */
	mvprintw(windowRows/2-2,(windowCols-14)/2,"              ");
	mvprintw(windowRows/2-1,(windowCols-14)/2,"              ");
	mvprintw(windowRows/2,(windowCols-14)/2,"   WARSHIPS   ");
	mvprintw(windowRows/2+1,(windowCols-14)/2,"              ");
	mvprintw(windowRows/2+2,(windowCols-14)/2,"              ");
	attroff(A_REVERSE);
	mvprintw(windowRows-2,(windowCols-10)/2, "©VB of ViS");
	// mvprintw(windowRows-1,(windowCols-16)/2, "rows %i, cols %i", windowRows, windowCols);
	refresh();
	getch();	/* wait for user */
	clear();
}

void splash(char *player, char *text)
{
	clear();
	int iterations = strlen(player) + 2;
	char blank[32] = "";
	while(iterations)
	{
		strcat(blank, " ");
		iterations--;
	}
	attron(A_REVERSE);	/* enable reverse terminal attribute */
	mvprintw(windowRows/2-1,(windowCols-strlen(blank))/2, "%s", blank);
	mvprintw(windowRows/2,(windowCols-strlen(player)-2)/2," %s ", player);
	mvprintw(windowRows/2+1,(windowCols-strlen(blank))/2, "%s", blank);
	attroff(A_REVERSE);
	mvprintw(windowRows/2+4,(windowCols-strlen(text))/2, text);
	refresh();
	getch();	/* wait for user */
	clear();
}

void wait()
{
	mvprintw(windowRows-3,(windowCols-12)/2, "PRESS RETURN");
	getch();
}

int main()
{
/*--- Game init phase ---*/
	char player1[10][10];
	char player2[10][10];
	mapClear(player1);	/* prepare map for game */
	mapClear(player2);

/*--- Screen init phase ---*/
	initscr();	/* Start curses mode */
	getmaxyx(stdscr,windowRows,windowCols);	/* get the number of rows and columns */
	start_color();			/* Start color functionality	*/
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	intro();

/*--- Ship placement phase ---*/
	splash("PLAYER 1", "SETUP");
	mapDraw(player1, player2, 1);
	for (int i = 0; i < 7; i++)	/* Player 1 ships setup */
	// for (int i = 0; i < 3; i++)	/********** for testing **********/
	{
		Coord put = getCoords();
		mapPut(player1, put);
		mapDraw(player1, player2, 1);
	}
	wait();
	splash("PLAYER 2", "SETUP");
	mapDraw(player2, player1, 1);
	for (int i = 0; i < 7; i++)	/* Player 2 ships setup */
	// for (int i = 0; i < 3; i++)	/********** for testing **********/
	{
		Coord put = getCoords();
		mapPut(player2, put);
		mapDraw(player2, player1, 1);
	}
	wait();

/*--- Game loop ---*/
	int game = 0;
	Coord attac;
	while(!game)
	{
		splash("PLAYER 1", "ATTACK");
		mapDraw(player1, player2, 2);
		attac = getCoords();
		mapShoot(player2, attac);
		mapDraw(player1, player2, 2);
		wait();
		if (!gameCheck(player2))
		{
			game = 1;
			break;
		}

		splash("PLAYER 2", "ATTACK");
		mapDraw(player2, player1, 2);
		attac = getCoords();
		mapShoot(player1, attac);
		mapDraw(player2, player1, 2);
		wait();
		if (!gameCheck(player1))
			game = 2;
	}
/*--- Wrap up ---*/
	char winner[9] = {'P', 'L', 'A', 'Y', 'E', 'R', ' ', game+'0', '\0'};
	splash(winner, "WINS THE GAME");
	endwin();	/* End curses mode */

	return 0;
}
